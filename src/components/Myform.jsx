import React, { Component } from 'react'
import { Row, Col, Form, Button, Table } from 'react-bootstrap'
export default class Myform extends Component {
    render() {
        return (
            <div>
                <br />
                <Row>
                    <Col>
                        <Row  className="justify-content-md-center">
                            <Col >
                            <div className="myImg">
                                <img src="./images/profile.png"   alt="" />
                            </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col><h2 className="text-center">Create Account</h2></Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form>

                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Username</Form.Label>
                                        <Form.Control type="text" placeholder="Username" />
                                        {/* <Form.Text className="text-muted"> We'll never share your email with anyone else.</Form.Text> */}
                                    </Form.Group>

                                    <Form.Group controlId="formBasicCheckbox">
                                        <Form.Label>Gender</Form.Label> <br />
                                        {/* <Form.Radio type="radio" name="gender"  label="Male" /> 
                                        <Form.Radio type="radio" name="gender" label="Female  " />  */}
                                        <input type="radio" name="gender" id="" value="Male" checked /> Male {' '}
                                        <input type="radio" name="gender" id="" value="Female"/> Female
                                    </Form.Group>
        
                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Email</Form.Label>
                                        <Form.Control type="email" placeholder="email" />
                                        {/* <Form.Text className="text-muted"> We'll never share your email with anyone else.</Form.Text> */}
                                    </Form.Group>

                                    <Form.Group controlId="formBasicPassword">
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control type="password" placeholder="Password" />
                                    </Form.Group>

                                    <Button variant="primary" type="submit">Submit</Button>
                                </Form>
                            </Col>
                        </Row>
                    </Col>
                    <Col xs lg="8">
                        <Row>
                            <Col>
                                <Table striped bordered hover>
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Gender</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Mark</td>
                                            <td>Otto</td>
                                            <td>@mdo</td>
                                        </tr>

                                    </tbody>
                                </Table>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>
        )
    }
}
