
import React from 'react'
import { Navbar, Nav, Form, FormControl, Button, Container } from 'react-bootstrap'
function Header() {
    return (
        <Container>
            <Navbar>
                <Navbar.Brand href="#home">KSHRD Students</Navbar.Brand>
                <Navbar.Toggle />
                <Navbar.Collapse className="justify-content-end">
                    <Navbar.Text>
                        Signed in as: <a href="#login">Mark Otto</a>
                    </Navbar.Text>
                </Navbar.Collapse>
            </Navbar>
        </Container>
    )
}
export default Header;