import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './components/Myheader';
import { Container } from 'react-bootstrap';
import Myform from './components/Myform'

function App() {
  return (
    <div>
      <Header />
      <Container>
        <Myform />
      </Container>
      {/* <h1 className="text-center">Hello HomeWork</h1> */}
    </div>

  );
}

export default App;
